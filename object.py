from path import Path
from math import sin, cos, pi, pow, sqrt


class Object:

    def __init__(self, path, path_type="unknown"):
        self.path = path
        self.path.parent = self
        self.pos = path.coords[0][0]
        self.vertices = []
        self.path_type = path_type
        self.spotted_path = Path(path.coords[0][0], 0, 0, (0, 0, 0))
        self.spotted_path.coords.clear()
        self.analyzed = False

    def update(self):
        if self.path_type == "breach":
            self.path.breach()
        elif self.path_type == "scouting":
            self.path.scouting()
        elif self.path_type == "searching":
            self.path.searching()

        self.pos = self.path.last()[0]

    def analyze(self):
        if not self.analyzed & len(self.spotted_path.coords) >= 10:
            # Checking if "SCOUTING"
            courses = set()
            for spot in self.spotted_path.coords:
                courses.add(spot[2])

            if len(courses) <= 3:
                # SCOUTING
                gen_course = 361
                width = -1
                length = 9999
                # Defining general course
                for c1 in courses:
                    has_counter = False
                    for c2 in courses:
                        if abs(c1 - c2) == 180:
                            has_counter = True
                            break
                        if not has_counter:
                            gen_course = c1

                # Calculating width
                angle = pi * (450 - gen_course) / 180
                for spot1 in self.spotted_path.coords:
                    for spot2 in self.spotted_path.coords:
                        displayced_x = spot1[0][0] - spot2[0][0]
                        displayced_y = spot1[0][1] - spot2[0][1]
                        transformed_x = displayced_x * cos(angle) - displayced_y * sin(angle)
                        transformed_y = displayced_x * sin(angle) + displayced_y * cos(angle)
                        # print("Displaced x:", displayced_x, "y:", displayced_y)
                        # print("Transformed x:", transformed_x, "y:", transformed_y)
                        if abs(transformed_x) > width: width = abs(transformed_x)
                        if abs(spot1[2] - spot2[2]) == 180:
                            if abs(transformed_y) < length: length = abs(transformed_y)
                if width != -1 & length != 9999:
                    print("Operation type: scouting")
                    print("General course:", gen_course)
                    print("Width:", width)
                    print("Length:", length)
                    self.analyzed = True

            if not self.analyzed:
                # BREACH
                mean = 0
                for c in courses:
                    mean += c
                mean /= len(courses)

                print("Mean course:", self.spotted_path.mean("c"))
                print("Mean course 2:", mean)
                print("Final:", (mean + self.spotted_path.mean("c")) / 2)



        else:
            print("Not enough data:", len(self.spotted_path.coords))
