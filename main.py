from OpenGL.GL import *
# from OpenGL.GLU import *
from OpenGL.GLUT import *
from math import cos, sin, sqrt, pi

from path import Path
from object import Object

import random

width = 1600
height = 900
objects = []
mouse_cache = []
key_cache = []
pause = False
mouse = (0, 0)
d_mouse = (0, 0)
chosen = None
camera = (0, 0)
time_warp = 1
time = 0

spots = []


def refresh2d():
    global width, height
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glOrtho(0.0, width, 0.0, height, 0.0, 1.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()


def draw_rect(x, y, w, h):
    glBegin(GL_QUADS)
    glVertex2f(x, y)
    glVertex2f(x + w, y)
    glVertex2f(x + w, y + h)
    glVertex2f(x, y + h)
    glEnd()


def gen_circle(r, div):
    vertices = []
    for i in range(0, div):
        angle = i * pi/(div/2)
        vertices += [(r * cos(angle), r * sin(angle))]
    return vertices


def draw_circle(x, y, div, vertices):
    for i in range(0, div):
        glBegin(GL_TRIANGLE_STRIP)
        glVertex2d(x, y)
        glVertex2d(x + vertices[i][0], y + vertices[i][1])
        if i != div-1:
            glVertex2d(x + vertices[i + 1][0], y + vertices[i + 1][1])
        else:
            glVertex2d(x + vertices[0][0], y + vertices[0][1])

        glEnd()


def draw_line(vertices):
    if len(vertices) <= 1:
        return
    glBegin(GL_LINE_STRIP)
    for coord in vertices:
        glVertex2d(coord[0][0], coord[0][1])
    glEnd()


def key_pressed(*args):
    global key_cache, time_warp, chosen, time
    if args[0] not in key_cache:
        key_cache += [args[0]]
    if args[0] == b'\x1b':
        sys.exit(0)
    elif args[0] == b' ':
        global pause
        pause = not pause
        print('Pause ')
        print(pause)
    elif args[0] == b'+':
        increase_time_warp()
        print('TimeWarp')
        print(time_warp)
    elif args[0] == b'-':
        decrease_time_warp()
        print('TimeWarp')
        print(time_warp)
    elif args[0] == b'q':
        print('TIME:', time)
        print('REAL MEAN')
        if chosen is not None:
            print("x:", chosen.path.mean("x"), "; y:", chosen.path.mean("y"),
                  "v:", chosen.path.mean("v"), "; c:", chosen.path.mean("c"))
        print('REAL RMS')
        if chosen is not None:
            print("x:", chosen.path.root_mean_square("x"), "; y:", chosen.path.root_mean_square("y"),
                  "v:", chosen.path.root_mean_square("v"), "; c:", chosen.path.root_mean_square("c"))
        print('')
    elif args[0] == b'w':
        print('TIME:', time)
        print('DATA:', len(chosen.spotted_path.coords))
        print('SPOTTED MEAN')
        if chosen is not None:
            print("x:", chosen.spotted_path.mean("x"), "; y:", chosen.spotted_path.mean("y"),
                  "v:", chosen.spotted_path.mean("v"), "; c:", chosen.spotted_path.mean("c"))
        print('SPOTTED RMS')
        if chosen is not None:
            print("x:", chosen.spotted_path.root_mean_square("x"), "; y:", chosen.spotted_path.root_mean_square("y"),
                  "v:", chosen.spotted_path.root_mean_square("v"), "; c:", chosen.spotted_path.root_mean_square("c"))
        print('')
    elif args[0] == b'e':
        print('Path')
        for obj in objects:
            print(obj.path.coords)
    elif args[0] == b'r':
        print('Analyzing')
        for obj in objects:
            obj.analyzed = False
            obj.analyze()
    else:
        return


def key_released(*args):
    global key_cache
    if args[0] in key_cache:
        key_cache.remove(args[0])


def mouse_pressed(*args):
    global mouse, height, chosen, key_cache, mouse_cache

    if args[1] == 0:
        if args[0] not in mouse_cache:
            mouse_cache += [args[0]]
    else:
        if args[0] in mouse_cache:
            mouse_cache.remove(args[0])

    if args[0] == 0:
        if args[1] == 0:
            chosen = None
            for obj in objects:
                dist = sqrt((mouse[0] - obj.pos[0])**2 + (mouse[1] - obj.pos[1])**2)
                if dist <= 25:
                    chosen = obj
                    break

    if chosen is None:
        if args[0] == 0:
            if args[1] == 0:
                if b'q' in key_cache:
                    add_object(mouse, 10, 20)


def mouse_move(*args):
    global mouse, d_mouse
    d_mouse = (mouse[0] - args[0], mouse[1] - (height - args[1]))
    mouse = (args[0], height - args[1])


def init():
    global width, height
    glutInit()
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH)
    glutInitWindowSize(width, height)
    glutInitWindowPosition(100, 100)
    glutCreateWindow(b'Oi')
    glutKeyboardFunc(key_pressed)
    glutKeyboardUpFunc(key_released)
    glutMouseFunc(mouse_pressed)
    glutPassiveMotionFunc(mouse_move)
    glutDisplayFunc(draw)
    glutIdleFunc(update)

    add_object(Path((100, 100), 0.3, 75, (45, 20, 30)), "breach")
    # add_object(Path((200, 200), 0.7, 135, (45, 40, 20)), "scouting")
    # add_object(Path((300, 300), 0.7, 40, (60, 0, 0)), "searching")

    glutMainLoop()


def draw():
    global width, height, chosen, d_mouse, mouse_cache, spots
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glLoadIdentity()
    refresh2d()

    # Draw
    for obj in objects:
        glColor3d(0.5, 0.5, 0.5)
        draw_line(obj.path.coords)
        glColor3d(1, 0.5, 0.5)
        draw_line(obj.spotted_path.coords)
        if obj == chosen:
            glColor3d(1, 0.5, 0.5)
        else:
            glColor3d(1, 1, 1)
        draw_circle(obj.pos[0] - camera[0], obj.pos[1] - camera[1], 32, obj.vertices)

    glutSwapBuffers()


def update():
    global time_warp, time, spots
    # Input
    if 2 in mouse_cache:
        print(d_mouse)

    # Update
    if not pause:
        for obj in objects:
            obj.update()
            if random.uniform(0, 1) <= 0.02:
                spots += [(obj, obj.path.last(), time)]
                obj.spotted_path.coords.append(obj.path.last())

    time += 1

    draw()


def add_object(path, path_type="breach"):
    global objects
    new_object = Object(path, path_type)
    new_object.vertices = gen_circle(10, 32)
    objects += [new_object]


def increase_time_warp():
    global time_warp
    increment = 0
    if 1 <= time_warp < 10:
        increment = 1
    elif 10 <= time_warp < 100:
        increment = 10
    elif 100 <= time_warp < 1000:
        increment = 100
    elif 1000 <= time_warp < 10000:
        increment = 1000
    elif 10000 <= time_warp < 100000:
        increment = 10000
    elif 100000 <= time_warp < 1000000:
        increment = 100000

    time_warp += increment


def decrease_time_warp():
    global time_warp
    decrement = 0
    if 1 < time_warp <= 10:
        decrement = 1
    elif 10 < time_warp <= 100:
        decrement = 10
    elif 100 < time_warp <= 1000:
        decrement = 100
    elif 1000 < time_warp <= 10000:
        decrement = 1000
    elif 10000 < time_warp <= 100000:
        decrement = 10000
    elif 100000 < time_warp <= 1000000:
        decrement = 100000

    time_warp -= decrement

init()

