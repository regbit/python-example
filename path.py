from math import sin, cos, pi, pow, sqrt
import random


class Path:

    def __init__(self, pos=(0.0, 0.0), vel=0.0, course=0.0, path_params=(0, 0, 0)):
        self.coords = []
        self.coords.append((pos, vel, course))
        self.turned = False
        self.parent = None
        self.last_turn = 0
        self.path_params = path_params

    def last(self):
        return self.coords[-1]

    def breach(self):
        gen_course = self.path_params[0]
        width = self.path_params[1]
        delta_course = self.path_params[2]

        angle = pi * (450 - self.last()[2]) / 180
        new_x = self.last()[0][0] + self.last()[1] * cos(angle)
        new_y = self.last()[0][1] + self.last()[1] * sin(angle)
        course = self.last()[2]

        displayced_x = new_x - self.coords[0][0][0]
        displayced_y = new_y - self.coords[0][0][1]
        angle = pi * (450 - gen_course) / 180
        transformed_x = displayced_x * cos(angle) - displayced_y * sin(angle)
        transformed_y = displayced_x * sin(angle) + displayced_y * cos(angle)
        # print("Displaced x:", displayced_x, "y:", displayced_y)
        # print("Transformed x:", transformed_x, "y:", transformed_y)
        if abs(transformed_y) >= abs(width):
            a = abs(transformed_y) - abs(width)
            b = self.last()[1] - a
            angle = pi * (450 - self.last()[2]) / 180
            new_x = self.last()[0][0] + b * cos(angle)
            new_y = self.last()[0][1] + b * sin(angle)

            if not self.turned:
                course -= delta_course + random.uniform(-delta_course / 10, delta_course / 10)
            else:
                course += delta_course + random.uniform(-delta_course / 10, delta_course / 10)

            self.coords.append(((new_x, new_y), self.last()[1], course))
            self.turned = not self.turned
            angle = pi * (450 - self.last()[2]) / 180
            new_x = self.last()[0][0] + a * cos(angle)
            new_y = self.last()[0][1] + a * sin(angle)
        self.coords.append(((new_x, new_y), self.last()[1], course))

    def scouting(self):
        gen_course = self.path_params[0]
        width = self.path_params[1]
        length = self.path_params[2]

        angle = pi * (450 - self.last()[2]) / 180
        new_x = self.last()[0][0] + self.last()[1] * cos(angle)
        new_y = self.last()[0][1] + self.last()[1] * sin(angle)
        course = self.last()[2]
        delta = sqrt(pow(self.coords[self.last_turn][0][0] - new_x, 2) +
                     pow(self.coords[self.last_turn][0][1] - new_y, 2))
        if gen_course != self.last()[2]:
            if delta >= width:
                a = delta - width
                b = self.last()[1] - a

                new_x = self.last()[0][0] + b * cos(angle)
                new_y = self.last()[0][1] + b * sin(angle)

                if gen_course < self.coords[self.last_turn][2]:
                    course -= 90
                elif gen_course > self.coords[self.last_turn][2]:
                    course += 90

                self.coords.append(((new_x, new_y), self.last()[1], course))
                self.last_turn = len(self.coords) - 1
                angle = pi * (450 - self.last()[2]) / 180
                new_x = self.last()[0][0] + a * cos(angle)
                new_y = self.last()[0][1] + a * sin(angle)
        else:
            if delta >= length:
                a = delta - length
                b = self.last()[1] - a

                new_x = self.last()[0][0] + b * cos(angle)
                new_y = self.last()[0][1] + b * sin(angle)
                if gen_course < self.coords[self.last_turn - 1][2]:
                    course -= 90
                elif gen_course > self.coords[self.last_turn - 1][2]:
                    course += 90

                self.coords.append(((new_x, new_y), self.last()[1], course))
                self.last_turn = len(self.coords) - 1
                angle = pi * (450 - self.last()[2]) / 180
                new_x = self.last()[0][0] + a * cos(angle)
                new_y = self.last()[0][1] + a * sin(angle)
        self.coords.append(((new_x, new_y), self.last()[1], course))

    def searching(self):
        radius = self.path_params[0]
        angle = pi * (450 - self.last()[2]) / 180
        new_x = self.last()[0][0] + self.last()[1] * cos(angle)
        new_y = self.last()[0][1] + self.last()[1] * sin(angle)
        course = self.last()[2]
        delta = sqrt(pow(self.coords[0][0][0] - new_x, 2) +
                     pow(self.coords[0][0][1] - new_y, 2))

        if abs(delta) > radius:
            b = abs(delta) - abs(radius)
            a = self.last()[1] - b
            new_x = self.last()[0][0] + a * cos(angle)
            new_y = self.last()[0][1] + a * sin(angle)
            course = (course + 180) % 360 + random.uniform(-45, -15) if random.uniform(0, 1) >= 0.5 \
                else random.uniform(45, 15)
            self.coords.append(((new_x, new_y), self.last()[1], course))
            self.last_turn = len(self.coords) - 1
            angle = pi * (450 - self.last()[2]) / 180
            new_x = self.last()[0][0] + b * cos(angle)
            new_y = self.last()[0][1] + b * sin(angle)
        self.coords.append(((new_x, new_y), self.last()[1], course))

    def mean(self, param_name):
        mean = 0
        for i in self.coords:
            if param_name == "x":
                mean += i[0][0]
            elif param_name == "y":
                mean += i[0][1]
            elif param_name == "v":
                mean += i[1]
            elif param_name == "c":
                mean += i[2]
        mean /= len(self.coords)

        return mean

    def root_mean_square(self, param_name):
        mean = self.mean(param_name)

        out = 0
        for i in self.coords:
            if param_name == "x":
                out += pow(i[0][0] - mean, 2)
            elif param_name == "y":
                out += pow(i[0][1] - mean, 2)
            elif param_name == "v":
                out += pow(i[1] - mean, 2)
            elif param_name == "c":
                out += pow(i[2] - mean, 2)

        return sqrt(out / len(self.coords))
